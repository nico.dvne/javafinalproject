package com.Nicod.JAVA;

import com.Nicod.JAVA.Client.IHM.Form;

import javax.swing.*;

public class MainProgram {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Form");
        frame.setContentPane(new Form().getjPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setVisible(true);
    }
}
