package com.Nicod.JAVA.Client.IHM;

import com.Nicod.JAVA.Client.ClientSide;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form {
    private JPanel jPanel;
    private JButton btn_enregistrer;
    private JTextField champ_saisie;

    public Form() {
        btn_enregistrer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    if ((champ_saisie.getText() == null || champ_saisie.getText().equals(""))) {
                        throw new Exception("Impossible de saisir une marque sans nom");
                    }
                        ClientSide.run(champ_saisie.getText());
                }
                catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(null, "Une erreur est survenue. Veuillez essayer à nouveau");
                    System.exit(-1);
                }
                JOptionPane.showMessageDialog(null, "Marque enregistrée en base de donnée");
                champ_saisie.setText("");
            }
        });
    }

    public JPanel getjPanel()
    {
        return this.jPanel;
    }
}
