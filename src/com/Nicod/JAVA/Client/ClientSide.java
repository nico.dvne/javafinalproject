package com.Nicod.JAVA.Client;


import com.Nicod.JAVA.Server.Server;

import java.io.*;
import java.net.Socket;

public class ClientSide {

    public static void run(String brand_name) throws Exception{

        Socket socket = null;
        try{
            socket = new Socket("localhost", Server.NUM_PORT);
        }
        catch(Exception ex)
        {
            throw new Exception("Connexion au serveur échouée");
        }

        OutputStream outputStream = null;

        try{
            outputStream = socket.getOutputStream();
        }
        catch(Exception ex)
        {
            throw new Exception("Creation outputStream a échouée");
        }

        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(new BufferedOutputStream(outputStream));
        } catch (IOException e) {
            throw new Exception("objectOutputStream failed");
        }

        try{
            objectOutputStream.writeObject(brand_name);
        }
        catch(Exception ex)
        {
            throw new Exception("Ecriture en DB failed");
        }

        try{
            objectOutputStream.close();
            outputStream.close();
            socket.close();
        }
        catch (Exception ex)
        {
            throw new Exception("Fermeture echouée");
        }
    }
}
