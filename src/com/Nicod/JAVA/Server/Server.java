package com.Nicod.JAVA.Server;

import com.Nicod.JAVA.Models.Brand;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int NUM_PORT = 1234;

    public static void main(String[] args) {

        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(Server.NUM_PORT);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        Socket socket = null;

        try {
            socket = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-2);
        }

        InputStream inputStream = null;

        try{
            inputStream = socket.getInputStream();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.exit(-3);
        }

        ObjectInputStream objectInputStream = null;

        try {
            objectInputStream = new ObjectInputStream(new BufferedInputStream(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String brand_name = null;

        try{
            brand_name = (String) objectInputStream.readObject();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.exit(-4);
        }
        Brand brand = null;
        try{
            brand = new Brand(brand_name);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.exit(-5);
        }

        Connexion con = null;

        try{
            con = new Connexion();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.exit(-6);
        }

        try{
            con.addBrand(brand);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.exit(-7);
        }

        try{
            objectInputStream.close();
            socket.close();
            serverSocket.close();
            con.Disconnect();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(-8);
        }





    }
}
