package com.Nicod.JAVA.Server;

import com.Nicod.JAVA.Models.Brand;

import java.sql.*;

public class Connexion {

    private final String TABLE_URL = "jdbc:mysql://5kage.xyz:3306/java_project_lprgi_davenne_schamel";
    private final String USER = "java_project_davenne_schamel";
    private final String PASSWORD = "java_password_davenne_schamel";
    private final String TABLE_NAME = "Brand";

    private Connection connection;
    private Statement stmt;


    public Connexion()
    {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(TABLE_URL, USER, PASSWORD);
            this.stmt = connection.createStatement();
            System.out.println("Connexion à la db réussie");
        }
        catch (Exception ex)
        {
            System.out.println("Erreur de la connexion a la db");
        }
    }

    public void Disconnect()
    {
        try {
            this.connection.close();
            this.stmt.close();
            System.out.println("Db is closed");
        } catch (SQLException throwables) {
            System.out.println("Error during closing db");
        }
    }

    public void addBrand(Brand brand)
    {
        String sql_request = "";
        sql_request += "INSERT INTO " + TABLE_NAME + " (name) VALUES (";
        sql_request += "'" + brand.getName() + "')";
        try{
            this.stmt.execute(sql_request);
            System.out.println("New brand was insert");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("Insert failed");
        }

    }

}
