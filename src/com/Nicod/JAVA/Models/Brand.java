package com.Nicod.JAVA.Models;

public class Brand {

    private String name;

    public Brand(String name)
    {
        this.name = name;
    }

    public void setName(String new_name)
    {
        this.name = new_name;
    }

    public String getName()
    {
        return this.name;
    }
}
