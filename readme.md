Projet réalisé par Yacine Schamel et Nicolas Davenne

Pour lancer le projet : 

Il faut lancer le serveur puis lancer le programme :
	Dans le fichier Server.java, lancer la méthode main
	Dans MainProgram.java, lancer la méthode main.

Dans le formulaire, si la saisie est vide ou qu'il y a un probleme avec le serveur, 
un message d'erreur apparait et le formulaire se ferme.
Si la saisie en base fonctionne, un message de validation apparait, le champ texte se vide et on peut saisir 
une chaine de caractère a nouveau. Cependant, pour chaque saisie dans le champ de saisie, il faut relancer le serveur.
